function reportExecuteScriptError(error) {
    document.querySelector("#error").classList.remove("hidden");
    console.error(`Erreur d'exécution du script de contenu beastify : ${error.message}`);
}

browser.tabs.executeScript({file: "/injected_scripts/injected.js"})
    .then(listenForClicks)
    .catch(reportExecuteScriptError);

function listenForClicks() {
    document.addEventListener("click", () => {
        function reportError(error) {
            console.error(`Beastify impossible : ${error}`);
        }

        function reportOK(tabs) {
            console.error(`OK`);
            browser.tabs.sendMessage(tabs[0].id, {});
        }

        browser.tabs.query({active: true, currentWindow: true})
            .then(reportOK)
            .catch(reportError);
    });
}


