let keySentences = [
    "Je n'accepte rien",
    'Continuer sans accepter',
    'Refuser',
    'Refuser tout',
    'Tout refuser',
    'Refuser les cookies',
    'Refuser et fermer',
    'Rejeter',
    'Rejeter tous les cookies',
    'Uniquement les cookies nécessaires',
    'Non merci'
];

let click = (sequence) => {
    console.log(`Start searching ${sequence}`)
    let elms = document.getElementsByTagName('*');
    for (const elm of elms) {
        let children = elm.childNodes;
        for (const child of children) {
            if (child.nodeType === 3 && child.nodeValue && !child.hasChildNodes()) {
                for (const sentence of keySentences) {
                    const value = child.nodeValue;
                    if (value.match(sentence)) {
                        console.log('type', child.nodeType);
                        console.log(child);
                        const parent = child.parentNode;
                        console.log(parent);
                        console.log('matched sentence', sentence);
                        parent.click();
                        console.log('found');
                    }
                }
            }
        }
    }
}

// Support of axeptio cookies
const observer = new MutationObserver(mutations => {
    mutations.forEach(mutation => {
        if (mutation.addedNodes.length) {
            mutation.addedNodes.forEach(node => {
                if (node.id && node.id === "axeptio_overlay") {
                    node.remove()
                }
            })
        }
    })
})

observer.observe(document.getElementsByTagName("body")[0], {childList: true});

click("instant");

setTimeout(click, 3000, "after 3 sec");
